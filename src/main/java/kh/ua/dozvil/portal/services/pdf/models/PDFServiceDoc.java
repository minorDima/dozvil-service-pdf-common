package kh.ua.dozvil.portal.services.pdf.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by minor on 13.09.17.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PDFServiceDoc {
    private long id;
    private String name;
    private String link;
    private String linkName;
}
