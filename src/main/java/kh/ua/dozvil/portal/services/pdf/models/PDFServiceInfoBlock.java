package kh.ua.dozvil.portal.services.pdf.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by Єрмакі on 10.09.2017.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PDFServiceInfoBlock {
    private String name;
    private String type;
    private List<PDFServiceInfoBlockData> items;

}
