package kh.ua.dozvil.portal.services.pdf.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PDFOpenOfficeService {
    private Long id;
    private String title;
    private String categoryPerson;
    private String listOfDocs;
    private String orderDocs;
    private String condService;
    private String condRefus;
    private String payService;
    private String timeServ;
    private String resultServ;
    private String processAnswer;
    private String actLegal;
    private String subjServ;
}
