package kh.ua.dozvil.portal.services.pdf.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PDFService {
    private long id;
    private String link;
    private String name;
    private String modificationDate;
    private boolean needPay;
    private List<PDFServiceInfoBlock> blocks;
    private List<PDFServiceDoc> docs;
    private List<String> providers;
    private String categoryOfApplicants;
    private String sampleURL;
    private List<String> districts;
    private String payDoc;
    private String wayToGet;
    private String termsOfService;
    private String term;
    private String additionalInfo;

    private String catalogName;
    private String catalogLink;
    private long catalogId;
}
