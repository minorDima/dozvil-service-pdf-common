package kh.ua.dozvil.portal.services.pdf.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by minor on 13.09.17.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PDFServiceInfoBlockData {
    private long id;
    private String link;
    private String linkName;
    private String name;
}
