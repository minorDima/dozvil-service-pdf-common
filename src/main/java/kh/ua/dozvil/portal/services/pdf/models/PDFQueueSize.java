package kh.ua.dozvil.portal.services.pdf.models;

public enum PDFQueueSize {
    BIG, SMALL
}
