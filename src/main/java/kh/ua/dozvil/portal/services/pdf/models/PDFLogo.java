package kh.ua.dozvil.portal.services.pdf.models;

import lombok.Getter;

public enum PDFLogo {
    DEFAULT("logo_d.jpg"),
    RCP("logo.jpg"),
    OPEN_OFFICE("logo_ooq.jpg");

    @Getter
    private String logo;

    PDFLogo(String logo){
        this.logo = logo;
    }
}
