package kh.ua.dozvil.portal.services.pdf.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PDFQueue {
    private PDFQueueSize size;
    private PDFLogo logo;
    private String serviceCenter;
    private String serviceCenterAddress;
    private String serviceName;
    private String date;
    private String time;
    private String number;
    private String sector;
    private Boolean paid;
}
