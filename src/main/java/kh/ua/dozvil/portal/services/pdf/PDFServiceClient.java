package kh.ua.dozvil.portal.services.pdf;

import kh.ua.dozvil.portal.services.pdf.models.PDFOpenOfficeService;
import kh.ua.dozvil.portal.services.pdf.models.PDFQueue;
import kh.ua.dozvil.portal.services.pdf.models.PDFService;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "PDFService")
public interface PDFServiceClient {

    @PostMapping("/service")
     ResponseEntity<byte[]> generatePdfForService(@RequestBody PDFService service);

    @PostMapping("/service/open-office")
     ResponseEntity<byte[]> generatePdfForOPenOfficeService(@RequestBody PDFOpenOfficeService service);

    @PostMapping("/queue")
     ResponseEntity<byte[]> generatePdfForQueue(@RequestBody PDFQueue queue);
}
